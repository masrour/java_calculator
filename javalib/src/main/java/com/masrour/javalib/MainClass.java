package com.masrour.javalib;


import java.util.*;

//import jdk.internal.org.objectweb.asm.TypeReference;

public class MainClass {

    public static int flag = 1;

    public static void main(String[] args) {

        int i = 1;

        int sizeOfNumbers = 0;
        int sizeOfOperators = 0;
        int sizeOfResults = 0;

        float num1 = 0;
        String op = "";
        float preResult = 0;
        float result = 0;
        String check = "+";


        //input class
        Scanner scan = new Scanner(System.in);
        //Object of Calculator Properties
        CalculatorClass calculator = new CalculatorClass();
        //
        FunctionsClass functions = new FunctionsClass();

        //first Element of Arrays
        calculator.setResult(0);
        calculator.setNumber(0);
        calculator.setOperator(check);

        sizeOfOperators = calculator.sizeOfOperators();

        functions.PrintNextLine("0");

        while (!calculator.getOperator(sizeOfOperators - 1).equals("=")) {


            if (i == 1 && flag == 1) {
                calculator.setResult(0);
            }

            if (flag == 1) {
                functions.PrintNextLine("Enter Operator ( + , - , * , / , = , +-*/(log , sin , cos) ) :");
                check = scan.next();

                //!check.equals("+") && !check.equals("-") && !check.equals("/") && !check.equals("*") && !check.equals("=") &&
                if ( !functions.patternMethod(check)) {
                    functions.PrintNextLine("Invalid Operator !!! Try Again");

                    continue;
                }

                calculator.setOperator(check);
            } else {
                flag = 1;
            }

            if (!calculator.getOperator(i).equals("=")) {

                boolean isNumeric = false;
                functions.PrintNextLine("Enter Number :");

                float checkNum = 0;

                if (isNumeric = scan.hasNextFloat()) {
                    checkNum = scan.nextFloat();
                } else {
                    functions.PrintNextLine("Invalid Number !!!");
                    scan.next();
                    flag = 0;
                    continue;
                }


                if (check.equals("/") && checkNum == 0) {
                    functions.PrintNextLine("Dividing by zero is not allowed !!!");
                    flag = 0;
                    continue;
                }
                calculator.setNumber(checkNum);

            } else {
                break;
            }


            sizeOfNumbers = calculator.sizeOfNumbers();
            sizeOfOperators = calculator.sizeOfOperators();
            sizeOfResults = calculator.sizeOfResults();

            num1 = calculator.getNumber(sizeOfNumbers - 1);
            op = calculator.getOperator(sizeOfOperators - 1);
            preResult = calculator.getResult(sizeOfResults - 1);


            result = functions.operation(num1, op, preResult);


            calculator.setResult(result);

            functions.PrintNextLine(preResult + "  " + op + "  " + num1 + " = " + result);
            i++;

        }


        functions.PrintSameLine("0");
        for (i = 1; i < sizeOfOperators; i++) {
            functions.PrintSameLine(calculator.getOperator(i) + "" + calculator.getNumber(i));
        }
        functions.PrintSameLine(" = " + result + "");

    }


}
