package com.masrour.javalib;
import java.util.regex.*;

public class FunctionsClass {

    public  int flag = 1;

    public boolean patternMethod(String word){
        Boolean result = false;

        if(Pattern.matches(".log", word)|| Pattern.matches(".sin", word)|| Pattern.matches(".cos", word)|| Pattern.matches(".", word)){
            result = true;


            switch (word.charAt(0)){
                case '+':
                case '-':
                case '*':
                case '/':
                case '=':
                    result = true ;
                    break;
                default:
                    result = false ;
            }
        }
        return result;
    }

   public  float operation(float num1,String op,float result){

        String tmOp;
        float tmpNum1=0;

       tmOp=op.substring(1);
       switch (tmOp) {
           case "log":
           tmpNum1 = (float) Math.log(num1);
               break;
           case "sin":
           tmpNum1 = (float) Math.sin(num1);
           break;
           case "cos":
           tmpNum1 = (float) Math.cos(num1);
           break;
           default:
               tmpNum1 = num1;
       }

       char firstOpLetter=op.charAt(0);
        switch (firstOpLetter) {
            case '+':
                result += tmpNum1 ;
                break;
            case '-':
                result -= tmpNum1 ;
                break;
            case '*':
                result *= tmpNum1 ;
                break;
            case '/':
                if(tmpNum1!=0) {
                    result /= tmpNum1;
                }
                break;

            default:
                return 0;
        }

        return result;

    }



    static void PrintNextLine(String text) {

        System.out.println(text);
    }

    static void PrintSameLine(String text) {

        System.out.print(text);
    }

}