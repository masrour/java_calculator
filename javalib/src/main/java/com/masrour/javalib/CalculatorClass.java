package com.masrour.javalib;

//import java.util.Scanner;
import java.util.*;

public class CalculatorClass {

    //    private float number ;
//    private String operator;
    private ArrayList<Float> numbers = new ArrayList<Float>();
    private ArrayList<String> operators = new ArrayList<String>();
    private ArrayList<Float> results = new ArrayList<Float>();



    float getNumber(int i) {
        return numbers.get(i);
    }

    void setNumber(/*int i,*/float number) {
        this.numbers.add(number);
        //this.numbers.add(i,number);
    }

    String getOperator(int i) {
        return operators.get(i);
    }

    void setOperator(String operator) {
        this.operators.add(operator);
    }

    float getResult(int i) {
        return results.get(i);
    }

    void setResult(float result) {
        this.results.add(result);
    }


    // Size of Arrays
    int sizeOfNumbers(){
        return this.numbers.size();
    }

    int sizeOfOperators(){
        return this.operators.size();
    }

    int sizeOfResults(){
        return this.results.size();
    }

}

